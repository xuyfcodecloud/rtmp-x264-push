package com.xyf.rtmppushdemo.param

data class AudioParam (val channelConfig:Int,val sampleRate:Int,val audioFormat:Int,val numChannel:Int)