package com.xyf.rtmppushdemo.param

data class VideoParam(
    val width: Int,
    val height: Int,
    val cameraId: Int,
    val bitRate: Int,
    val frameRate: Int
)