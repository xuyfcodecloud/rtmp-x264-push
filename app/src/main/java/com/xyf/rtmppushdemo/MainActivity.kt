package com.xyf.rtmppushdemo

import android.Manifest
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.os.*
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.util.Size
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.video.Recorder
import androidx.camera.video.Recording
import androidx.camera.video.VideoCapture
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.xyf.rtmppushdemo.camera.CameraHelp
import com.xyf.rtmppushdemo.camera.CameraType
import com.xyf.rtmppushdemo.databinding.ActivityMainBinding
import com.xyf.rtmppushdemo.listener.LiveStateChangeListener
import com.xyf.rtmppushdemo.param.AudioParam
import com.xyf.rtmppushdemo.param.VideoParam
import com.zackratos.ultimatebarx.ultimatebarx.statusBarOnly
import java.nio.ByteBuffer
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class MainActivity : BaseActivity<ActivityMainBinding>(),LiveStateChangeListener {

    var livePush: LivePusher? = null

    override fun initBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    private val mHandler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            if (msg.what == MSG_ERROR) {
                val errMsg = msg.obj as String
                if (!TextUtils.isEmpty(errMsg)) {
                    Toast.makeText(this@MainActivity, errMsg, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun init() {
        initPusher()
        initView()
    }

    private fun initView() {
        binding.btnPush.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                livePush?.startPush(LIVE_URL,this)

            }else{
                livePush?.stopPush()
            }
        }
        binding.btnMute.setOnCheckedChangeListener{_,isChecked->
            if (isChecked) {
                livePush?.setMute(true)

            }else{
                livePush?.setMute(false)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        livePush?.release()
    }

    private fun initPusher() {
        //视频参数
        val width = 640
        val height = 480
        val videoBitRate = 800000
        val videoFrameRate = 10

        val videoParam = VideoParam(
            width,
            height, CameraHelp.CAMERA_ID_BACK,
            videoBitRate, videoFrameRate
        )
        //音频参数
        val sampleRate = 44100
        val channelConfig = AudioFormat.CHANNEL_IN_STEREO
        val audioFormat = AudioFormat.ENCODING_PCM_16BIT
        val numChannels = 2
        val audioParam = AudioParam(channelConfig, sampleRate, audioFormat, numChannels)
        livePush =
            LivePusher(this, videoParam, audioParam, binding.surfaceCamera, CameraType.CAMERA2)

    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private const val LIVE_URL ="rtmp://live-push.bilivideo.com/live-bvc/?streamname=live_296906540_23200576&key=2f22e18e9b8ace7ed8d04a83762be658&schedule=rtmp&pflag=1"
        private const val MSG_ERROR = 100
    }

    override fun onError(msg: String) {
        mHandler.obtainMessage(MSG_ERROR,msg).sendToTarget()
    }
}