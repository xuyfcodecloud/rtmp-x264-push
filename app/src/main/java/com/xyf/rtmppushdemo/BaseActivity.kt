package com.xyf.rtmppushdemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.zackratos.ultimatebarx.ultimatebarx.statusBarOnly

abstract class BaseActivity<B : ViewBinding> : AppCompatActivity() {
    lateinit var binding: B

    abstract fun initBinding():B

    override fun onCreate(savedInstanceState: Bundle?) {
        statusBarOnly {
            transparent()
            light = true
        }
        super.onCreate(savedInstanceState)
        binding = initBinding()
        setContentView(binding.root)
        init()
    }
    abstract fun init()
}