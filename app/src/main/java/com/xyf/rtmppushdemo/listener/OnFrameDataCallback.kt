package com.xyf.rtmppushdemo.listener

import com.xyf.rtmppushdemo.camera.CameraType

interface OnFrameDataCallback {
    fun getInputSamples(): Int
    fun onAudioFrame(byte: ByteArray)
    fun onAudioCodecInfo(sampleRate: Int, channelCount: Int)
    fun onVideoFrame(byte: ByteArray, cameraId: Int)
    fun onVideoCodecInfo(width: Int, height: Int, frameRate: Int, bitrate: Int)
}