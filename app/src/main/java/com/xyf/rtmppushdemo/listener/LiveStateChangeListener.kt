package com.xyf.rtmppushdemo.listener

interface LiveStateChangeListener {
    fun onError(msg: String)
}