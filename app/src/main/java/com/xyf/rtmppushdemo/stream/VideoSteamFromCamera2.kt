package com.xyf.rtmppushdemo.stream

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.graphics.SurfaceTexture
import android.util.Size
import android.view.Surface
import android.view.TextureView
import android.view.View
import com.xyf.rtmppushdemo.camera.Camera2Helper
import com.xyf.rtmppushdemo.camera.Camera2Listener
import com.xyf.rtmppushdemo.listener.OnFrameDataCallback
import com.xyf.rtmppushdemo.param.VideoParam

class VideoSteamFromCamera2 constructor(
    var onFrameDataCallback: OnFrameDataCallback,
    surfaceView: View, var videoParam: VideoParam,
    var context: Context
) : VideoSteam, TextureView.SurfaceTextureListener, Camera2Listener {
    private var rotation = 0
    private var previewSize: Size? = null
    private var camera2Helper: Camera2Helper? = null
    private var textureView: TextureView = surfaceView as TextureView
    var isLiving: Boolean = false

    init {
        textureView.surfaceTextureListener = this
    }


    override fun startLive() {
        isLiving = true
    }


    override fun switchCamera() {
        camera2Helper?.switchCamera()
    }

    override fun stopLive() {
        isLiving = false
    }

    override fun release() {
        camera2Helper?.stop()
        camera2Helper?.release()
        camera2Helper = null
    }

    override fun onPreViewDegreeChanged(degree: Int) {
        updateVideoCodecInfo(degree)
    }

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
        startPreView()
    }

    private fun startPreView() {
        rotation = (context as Activity).windowManager.defaultDisplay.rotation
        camera2Helper = Camera2Helper.Builder()
            .cameraListener(this)
            .specificCameraId(Camera2Helper.CAMERA_ID_BACK)
            .context(context.applicationContext)
            .previewOn(textureView)
            .previewViewSize(Point(videoParam.width, videoParam.height))
            .rotation(rotation)
            .rotateDegree(getPreviewDegree(rotation))
            .build()
        camera2Helper?.start()
    }

    private fun getPreviewDegree(rotation: Int): Int {
        return when (rotation) {
            Surface.ROTATION_0 -> 90
            Surface.ROTATION_90 -> 0
            Surface.ROTATION_180 -> 270
            Surface.ROTATION_270 -> 180
            else -> -1
        }
    }

    override fun onSurfaceTextureSizeChanged(p0: SurfaceTexture, p1: Int, p2: Int) {}

    override fun onSurfaceTextureDestroyed(p0: SurfaceTexture): Boolean {
//        stopPreview()
        return false
    }

    override fun onSurfaceTextureUpdated(p0: SurfaceTexture) {}


    override fun onCameraOpened(previewSize: Size?, displayOrientation: Int) {
        this.previewSize = previewSize
        updateVideoCodecInfo(getPreviewDegree(rotation))
    }

    private fun updateVideoCodecInfo(previewDegree: Int) {
        camera2Helper?.updatePreviewDegree(previewDegree)
        previewSize?.let {
            var width = it.width
            var height = it.height
            if (previewDegree == 90 || previewDegree == 270) {
                var temp = width
                width = height
                height = temp
            }
            onFrameDataCallback.onVideoCodecInfo(
                width,
                height,
                videoParam.frameRate,
                videoParam.bitRate
            )
        }

    }

    override fun onPreviewFrame(yuvData: ByteArray?) {
        if (isLiving) {
            yuvData?.let {
                onFrameDataCallback.onVideoFrame(it,2)
            }
        }
    }

    override fun onCameraClosed() {

    }

    override fun onCameraError(e: Exception?) {

    }
}