package com.xyf.rtmppushdemo.stream

import android.view.SurfaceHolder
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

interface VideoSteam :LifecycleObserver{
    fun startLive()
    fun setPreViewDisplay(surfaceHolder: SurfaceHolder){}
    fun switchCamera()
    fun stopLive()
    fun release()
    fun onPreViewDegreeChanged(degree: Int)
}