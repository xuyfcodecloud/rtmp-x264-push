//
// Created by 随幻 on 2022/8/8.
//

#ifndef RTMPPUSHDEMO_AUDIOSTREAM_H
#define RTMPPUSHDEMO_AUDIOSTREAM_H



#include "rtmp/rtmp.h"
#include "include/faac/faac.h"
#include <sys/types.h>

class AudioStream {
    typedef void (*AudioCallback)(RTMPPacket *packet);

public:
    AudioStream();
    ~AudioStream();

    void setAudioEncInfo(int samplesInHz,int channels);
    void setAudioCallback(AudioCallback audioCallback);
    int getInputSamples();
    void encodeData(int8_t *data);
    RTMPPacket *getAudioTag();

private:
    AudioCallback audioCallback;
    int mChannels;
    faacEncHandle audioCodec = 0;
    u_long inputSamples;
    u_long maxOutputBytes;
    u_char *buffer = 0;
};


#endif //RTMPPUSHDEMO_AUDIOSTREAM_H
