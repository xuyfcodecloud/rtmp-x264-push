//
// Created by 随幻 on 2022/8/4.
//

#ifndef RTMPPUSHDEMO_PUSHINTERFACE_H
#define RTMPPUSHDEMO_PUSHINTERFACE_H
#include <android/log.h>

#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,"FrankLive",__VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,"FrankLive",__VA_ARGS__)

#define DELETE(obj) if(obj){ delete obj; obj = 0; }

#endif //RTMPPUSHDEMO_PUSHINTERFACE_H
